package com.soa.servicio;

import com.soa.modelo.Respuesta;

public interface IAbrirAplicacion {

	public Respuesta abrirGoogleChrome();
}
