package com.soa.utilidades;

public class Constantes {
	/*
	 * aplicaciones
	 */
	public static final String GOOGLE_CHROME = "GC";
	/*
	 * codigos de error especificos
	 */
	public static final int ERROR_APLICACION_NO_ENCONTRADA = 0;
	/*
	 * codigos de respuesta
	 */
	public static final String ERROR_FATAL = "ERROR";
	public static final String CORRECTO = "CORRECT";
}
