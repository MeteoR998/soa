package com.soa.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soa.modelo.Respuesta;
import com.soa.repositorio.IAbrirAplicacionesRepo;
import com.soa.utilidades.Constantes;

@Service
public class AbrirAplicacionImp implements IAbrirAplicacion {

	@Autowired
	private IAbrirAplicacionesRepo abrirAplicacion;
	@Override
	public Respuesta abrirGoogleChrome() {
		
		try {
			abrirAplicacion.abrirGoogleChrome();
		} catch (Exception e) {
			
		}
		
		Respuesta respuesta = new Respuesta();
		respuesta.setCodigo(Constantes.CORRECTO);
		respuesta.setMensaje("Se a abierto correctamente");
		
		return respuesta;
	}

	

}
