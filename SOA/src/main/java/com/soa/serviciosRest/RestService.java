package com.soa.serviciosRest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.soa.modelo.Error;
import com.soa.modelo.Respuesta;
import com.soa.servicio.IAbrirAplicacion;
import com.soa.utilidades.Constantes;

@RestController
public class RestService {
	@Autowired
	private IAbrirAplicacion abrirAplicacion;

	@GetMapping()
	@RequestMapping(value = "abrirAplicacion/{app}", method = RequestMethod.GET)
	public Respuesta obtenerPeticion(@PathVariable("app") String nombreAplicacion) {
		if (nombreAplicacion.equalsIgnoreCase(Constantes.GOOGLE_CHROME)) {
			Respuesta respuesta=abrirAplicacion.abrirGoogleChrome();
			return respuesta;
		
		}
		Error error= new Error();
		error.setCodigo(Constantes.ERROR_APLICACION_NO_ENCONTRADA);
		error.setMensaje("no he podido encontrar la aplicación");
		Respuesta respuesta=new Respuesta();
		respuesta.setError(error);
		respuesta.setCodigo(Constantes.ERROR_FATAL);
		return respuesta;
		

	}

}
