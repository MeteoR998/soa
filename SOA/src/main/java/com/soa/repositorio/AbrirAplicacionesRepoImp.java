package com.soa.repositorio;

import org.springframework.stereotype.Repository;

@Repository
public class AbrirAplicacionesRepoImp implements IAbrirAplicacionesRepo {

	@Override
	public void abrirGoogleChrome() throws Exception {

		String cmd = "cmd.exe /c START chrome";
		Runtime.getRuntime().exec(cmd);

	}

}
